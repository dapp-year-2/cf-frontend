import React from "react";
import  Banner  from "./banner";
import Content from "./content";
import LandingNavBar from "./LandingNavBar";

function LandingPage(){
    return(
        <>
            <LandingNavBar/>
            <Banner/>
            <Content/>
        </>
    );
}export default LandingPage;