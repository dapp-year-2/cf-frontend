import React, { useState } from 'react';
import "./css/setting.css";
import Illustration from '../img/Illustration.png';
import B1 from '../img/44.jpg';
import NavbarD from './navbarD';

const SettingDash = () => {

  const data1 = [
    { profilePic: B1, name: 'John Doe', email: 'john.doe@example.com' },
    { profilePic: Illustration, name: 'Jane Smith', email: 'jane.smith@example.com' },
    { profilePic: 'pic3.jpg', name: 'Alex Johnson', email: 'alex.johnson@example.com' },
  ];

  const userCount = data1.filter((item) => item).length;

  return (
    <div className="DashAll">
      <NavbarD />

        <div className="HeadingAd1">
            <h1>Hello Admin</h1>
            <p>It's good to see you again!</p>
        </div>

        <img src={Illustration} id="imgill1" />
        <form className='settingForm'>
            <label for="name">Name:</label><br/>
            <input type="text" id="name" name="name" /><br/>

            <label for="email">Email:</label><br/>
            <input type="email" id="email" name="email" /><br/>

            <label for="password">Password:</label><br/>
            <input type="password" id="password" name="password" /><br/>

            <label for="Cpassword">Confirm Password:</label><br/>
            <input type="password" id="Cpassword" name="Cpassword" /><br/>

            <input type="submit" id="setSubmit" value="Submit"/>

        </form>

     
    

    </div>
   
  );
};

export default SettingDash;
