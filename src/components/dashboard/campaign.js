import React, { useState } from 'react';
import './css/campaign.css';
import Illustration from '../img/Illustration.png';
import NavbarD from './navbarD';
import Modal from 'react-modal';

Modal.setAppElement('#root');

const CampaignDash = (props) => {
  let allcampaigns = [];
  let completedcampaigns = [];
  let activecampaigns = [];
  for (var i = 0; i < props.allCampaigns.length; i++) {
    allcampaigns.push(props.allCampaigns[i])
    props.allCampaigns[i].completed === true && completedcampaigns.push(props.allCampaigns[i])
    props.allCampaigns[i].completed === false && activecampaigns.push(props.allCampaigns[i])
  }

  const [filteredData, setFilteredData] = useState(allcampaigns);
  const [activeCategory, setActiveCategory] = useState('All');
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const handleCategoryClick = (category) => {
    setActiveCategory(category);

    if (category === 'All') {
      setFilteredData(allcampaigns);
    } else if (category === "Completed") {
      setFilteredData(completedcampaigns);
    } else {
      setFilteredData(activecampaigns)
    }
  };

  // const completedCount = allData.filter((item) => item.status === 'Completed').length;
  // const activeCount = allData.filter((item) => item.status === 'Active').length;

  const openModal = (item) => {
    setSelectedItem(item);
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };
  const modalStyles = {
    overlay: {
      // Styles for the overlay (background behind the modal)
      // ...
    },
    content: {
      // Styles for the modal content
      maxHeight: '80vh', // Set a maximum height for the content
      overflowY: 'auto', // Enable vertical scrolling
      // ...
    }
  }
  return (
    <div className="DashAll2" >
      <NavbarD />

      <div className="HeadingAd2">
        <h1>Hello Admin</h1>
        <p>It's good to see you again!</p>
      </div>

      <img src={Illustration} id="imgill2" alt='' />

      <div className="headingDD2" id="hd12">
        <h1>{completedcampaigns.length}</h1>
        <p>Campaigns</p>
        <p>Completed</p>
      </div>

      <div className="headingDD2" id="hd22">
        <strong>
          <h1>{activecampaigns.length}</h1>
        </strong>
        <p>Campaigns</p>
        <p>in progress</p>
      </div>

      <h2 id="Ttitle12">Campaigns</h2>
      <div className="categories2">
        <button
          className={activeCategory === 'All' ? 'active' : ''}
          onClick={() => handleCategoryClick('All')}
        >
          All
        </button>
        <button
          className={activeCategory === 'Completed' ? 'active' : ''}
          onClick={() => handleCategoryClick('Completed')}
        >
          Completed
        </button>
        <button
          className={activeCategory === 'Active' ? 'active' : ''}
          onClick={() => handleCategoryClick('Active')}
        >
          Active
        </button>
      </div>

      <table className="DashTable2" id="CampaignTable2">
        <thead>
          <tr>
            <th></th>
            <th>Title</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {filteredData.map((item, index) => (
            <tr key={index}>
              <td>
                <img src={item.image} alt="Banner Img" style={{ height: '6rem', borderRadius: '10px' }} />
              </td>
              <td>
                <h4 style={{ fontSize: "1rem" }}>{item.title}</h4>
              </td>
              <td>{item.status === true ? "Completed" : "Active"}</td>
              <td> <input type='button' value="View" onClick={() => openModal(item)} /> </td>
            </tr>
          ))}
        </tbody>
      </table>

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={modalStyles}
        contentLabel="View Campaign"
        className="Modal"
      >
        {selectedItem && console.log(selectedItem)}
        {selectedItem && (
          <div>
            <h2>{selectedItem.title}</h2>
            <img src={selectedItem.image} alt="Banner Img" style={{ height: '280px', borderRadius: '10px' }} />
            {selectedItem.description.split(/\n|\n\n/).map((p, i) => (
              <p style={{ textAlign: "justify", paddingInline: "3rem" }} key={i}>{p}</p>
            ))}
            <p>Status: {selectedItem.status === true ? "Completed" : "Active"}</p>
            <button onClick={closeModal}>Close</button>
          </div>
        )}
      </Modal>
    </div >
  );
};

export default CampaignDash;
